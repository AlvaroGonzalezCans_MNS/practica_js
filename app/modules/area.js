function Area () {
}

Area.prototype = {
	areaCuadrado: function (lado){
		return lado * lado;
	},

	areaCirculo: function (radio){
		return Math.PI.toFixed(2) * radio * radio;
	}

}
