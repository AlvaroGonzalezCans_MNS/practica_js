function Volumen () {
}

Volumen.prototype = {
	volumenCuadrado: function (lado, oArea){
		return oArea.areaCuadrado(lado) * lado;
	},

	volumenCirculo: function (radio, oArea){
		return oArea.areaCirculo(radio) * radio * ((4 / 3).toFixed(2));
	}
}; 